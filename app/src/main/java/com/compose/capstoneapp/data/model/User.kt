package com.compose.capstoneapp.data.model

data class User(
    val email: String = "",
    val name: String = "",
    val user_id: String = ""
) {
    fun toMap(): MutableMap<String, Any> {
        return mutableMapOf(
            "email" to email,
            "name" to name,
            "user_id" to user_id
        )
    }
}
