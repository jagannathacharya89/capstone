package com.compose.capstoneapp.data.model

data class AccessInfo(
    val country: String,
    val viewability: String
)
