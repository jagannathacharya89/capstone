package com.compose.capstoneapp.data.model

data class ImageLink(
    val smallThumbnail: String,
    val thumbnail: String
)
