package com.compose.capstoneapp.data.model

data class LoginResult(
    val isSucess: Boolean,
    val message: String
)
