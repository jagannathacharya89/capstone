package com.compose.capstoneapp.data.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName

data class FirebaseBook(
    @Exclude var id: String? = null,
    var title: String? = null,
    var authors: String? = null,
    var notes: String? = null,
    var rating: Int? = null,
    var publisher: String? = null,
    var publishedDate: String? = null,
    var description: String? = null,
    var pageCount: Int? = null,
    var categories: String? = null,
    var imgUrl: String? = null,
    var startTime: Timestamp? = null,
    var endTime: Timestamp? = null,
    var userId: String? = null,
    var googleBookId: String? = null,
    @get: PropertyName("favorite")
    @set: PropertyName("favorite")
    var isFavorite: Boolean = false,
    @get: PropertyName("reading")
    @set: PropertyName("reading")
    var isReading: Boolean = false,
    @get: PropertyName("read")
    @set: PropertyName("read")
    var isRead: Boolean = false
)
