package com.compose.capstoneapp.data.model

data class SearchInfo(
    val textSnippet: String
)
