package com.compose.capstoneapp.data.model

data class SaleInfo(
    val country: String,
    val saleability: String
)
