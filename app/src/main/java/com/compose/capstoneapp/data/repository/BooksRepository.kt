package com.compose.capstoneapp.data.repository

import com.compose.capstoneapp.data.api.BooksApi
import javax.inject.Inject

class BooksRepository @Inject constructor(private val api: BooksApi) {

    suspend fun fetchBooks(query: String) = api.fetchBooks(query)

    suspend fun fetchBookInfo(id: String) = api.fetchBookInfo(id)
}