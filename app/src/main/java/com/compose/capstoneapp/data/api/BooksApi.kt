package com.compose.capstoneapp.data.api

import com.compose.capstoneapp.data.model.Book
import com.compose.capstoneapp.data.model.BookResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface BooksApi {

    @GET("volumes")
    suspend fun fetchBooks(
        @Query("q") query: String
    ): Response<BookResponse>

    @GET("volumes/{bookId}")
    suspend fun fetchBookInfo(
        @Path("bookId") id: String
    ): Response<Book>
}