package com.compose.capstoneapp.data.model

data class BookResponse(
    val items: List<Book>
)
