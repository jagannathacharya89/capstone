package com.compose.capstoneapp.data.model

data class Book(
    val id: String,
    val accessInfo: AccessInfo,
    val saleInfo: SaleInfo,
    val searchInfo: SearchInfo,
    val volumeInfo: VolumeInfo
)
