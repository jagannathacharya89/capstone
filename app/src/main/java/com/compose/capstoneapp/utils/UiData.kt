package com.compose.capstoneapp.utils

sealed class UiData<T> {
    class Error<T>(val error: String): UiData<T>()
    class Loading<T>: UiData<T>()
    class None<T>: UiData<T>()
    class Success<T>(val data: T): UiData<T>()
}