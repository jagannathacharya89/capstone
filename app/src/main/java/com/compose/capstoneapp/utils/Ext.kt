package com.compose.capstoneapp.utils

import android.content.Context
import android.icu.text.DateFormat
import android.util.Patterns
import android.widget.Toast
import androidx.core.text.HtmlCompat
import com.google.firebase.Timestamp

fun String.fetchFirstName(): String {
    return if (contains(' ')) {
        split("\\s+".toRegex()).first()
    } else {
        this
    }
}

fun String.formatHtml(): String {
    return HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
}

fun Timestamp?.formatTimeStamp(): String {
    var date = ""
    this?.let {
        date = DateFormat.getInstance()
            .format(it.toDate())
            .toString()
            .split(",")[0]
    } ?: run {
        date = "N/A"
    }
    return date
}

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS
        .matcher(this)
        .matches()
}

fun List<String>?.joinString(): String {
    return if (this.isNullOrEmpty()) "N/A"
    else if (this.isEmpty()) "N/A"
    else this.joinToString(", ")
}

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Int?.toDoubleOrZero(): String {
    var rating = ""
    this?.let {
        rating = it.toDouble().toString()
    } ?: run {
        rating = "0.0"
    }
    return rating
}

fun Int?.toIntOrZero(): Int {
    var rating = 0
    this?.let {
        rating = it
    } ?: run {
        rating = 0
    }
    return rating
}

fun String?.toStringOrNA(): String {
    return this ?: "N/A"
}