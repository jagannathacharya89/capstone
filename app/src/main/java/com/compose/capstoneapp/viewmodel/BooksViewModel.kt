package com.compose.capstoneapp.viewmodel

import com.compose.capstoneapp.data.model.Book
import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.data.repository.BooksRepository
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.formatHtml
import com.compose.capstoneapp.utils.joinString
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
class BooksViewModel @Inject constructor(private val repository: BooksRepository): ViewModelBase() {

    private val _bookDeleted = MutableStateFlow<UiData<Boolean>>(UiData.None())
    val bookDeleted = _bookDeleted.asStateFlow()
    private val _books = MutableStateFlow<UiData<List<Book>>>(UiData.None())
    val books = _books.asStateFlow()
    private val _bookInfo = MutableStateFlow<UiData<Book>>(UiData.None())
    val bookInfo = _bookInfo.asStateFlow()
    private val _bookSaved = MutableStateFlow<UiData<Boolean>>(UiData.None())
    val bookSaved = _bookSaved.asStateFlow()
    private val _bookUpdated = MutableStateFlow<UiData<Boolean>>(UiData.None())
    val bookUpdated = _bookUpdated.asStateFlow()
    private val _firebaseBookInfo = MutableStateFlow<UiData<FirebaseBook>>(UiData.None())
    val firebaseBookInfo = _firebaseBookInfo.asStateFlow()

    init {
        fetchBooks("android")
    }

    fun addBook(book: Book) {
        launchCoroutineScope {
            _bookSaved.value = UiData.Loading()
            val firebaseBook = FirebaseBook(
                title = book.volumeInfo.title,
                authors = book.volumeInfo.authors.joinString(),
                notes = "",
                rating = 0,
                publisher = book.volumeInfo.publisher,
                publishedDate = book.volumeInfo.publishedDate,
                description = book.volumeInfo.description.formatHtml(),
                pageCount = book.volumeInfo.pageCount,
                categories = book.volumeInfo.categories.joinString(),
                imgUrl = book.volumeInfo.imageLinks.smallThumbnail,
                userId = FirebaseAuth.getInstance().currentUser?.uid,
                googleBookId = book.id
            )
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .add(firebaseBook)
                .addOnSuccessListener { docRef ->
                    db.collection("books")
                        .document(docRef.id)
                        .update(hashMapOf("id" to docRef.id) as Map<String, Any>)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                _bookSaved.value = UiData.Success(true)
                            } else {
                                _bookSaved.value = UiData.Error("Failed to save the book - ${task.exception?.message}")
                            }
                        }.addOnFailureListener {
                            _bookSaved.value = UiData.Error("Failed to save the book - ${it.message}")
                        }
                }.addOnFailureListener {
                    _bookSaved.value = UiData.Error("Failed to save the book - ${it.message}")
                }
        }
    }

    fun deleteBook(id: String) {
        launchCoroutineScope {
            _bookDeleted.value = UiData.Loading()
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .document(id)
                .delete()
                .addOnCompleteListener {
                    _bookDeleted.value = UiData.Success(true)
                }.addOnFailureListener {
                    _bookDeleted.value = UiData.Error("Failed to delete the book - ${it.message}")
                }
        }
    }

    fun fetchBooks(query: String) {
        launchCoroutineScope {
            _books.value = UiData.Loading()
            val response = repository.fetchBooks(query)
            if (response.isSuccessful) {
                response.body()?.let {
                    _books.value = UiData.Success(it.items)
                } ?: run {
                    _books.value = UiData.Error("No books found!")
                }
            } else {
                _books.value = UiData.Error("No books found!")
            }
        }
    }

    fun fetchBookInfo(id: String) {
        launchCoroutineScope {
            _bookInfo.value = UiData.Loading()
            val response = repository.fetchBookInfo(id)
            if (response.isSuccessful) {
                response.body()?.let {
                    _bookInfo.value = UiData.Success(it)
                } ?: run {
                    _bookInfo.value = UiData.Error("No details found!")
                }
            } else {
                _bookInfo.value = UiData.Error("No details found!")
            }
        }
    }

    fun fetchBookInfoFromFirebase(title: String) {
        launchCoroutineScope {
            _firebaseBookInfo.value = UiData.Loading()
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .get()
                .addOnSuccessListener { query ->
                    var book: FirebaseBook? = null
                    query.find { document ->
                        document.toObject(FirebaseBook::class.java).title == title
                    }?.also { document ->
                        book = document.toObject(FirebaseBook::class.java)
                    }
                    book?.let {
                        _firebaseBookInfo.value = UiData.Success(it)
                    }
                }.addOnFailureListener {
                    _firebaseBookInfo.value = UiData.Error("No details found - ${it.message}")
                }
        }
    }

    fun updateBook(
        id: String,
        comments: String,
        isRead: Boolean,
        isReading: Boolean,
        rating: Int,
        endTime: Timestamp?,
        startTime: Timestamp?
    ) {
        launchCoroutineScope {
            _bookUpdated.value = UiData.Loading()
            val updatedMap = hashMapOf(
                "notes" to comments,
                "reading" to isReading,
                "read" to isRead,
                "rating" to rating,
                "endTime" to if (isRead) Timestamp.now() else endTime,
                "startTime" to if (isReading) Timestamp.now() else startTime,
            ) as Map<String, Any?>
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .document(id)
                .update(updatedMap)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        _bookUpdated.value = UiData.Success(true)
                    } else {
                        _bookUpdated.value = UiData.Error("Failed to update the book - ${task.exception?.message}")
                    }
                }.addOnFailureListener {
                    _bookUpdated.value = UiData.Error("Failed to update the book - ${it.message}")
                }
        }
    }

    override fun onCreate() {}
}