package com.compose.capstoneapp.viewmodel

import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.data.model.User
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.fetchFirstName
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class HomeViewModel: ViewModelBase() {

    private val _books = MutableStateFlow<UiData<List<FirebaseBook>>>(UiData.None())
    val books = _books.asStateFlow()
    private val _bookUpdated = MutableStateFlow<UiData<Boolean>>(UiData.None())
    val bookUpdated = _bookUpdated.asStateFlow()
    private val _userData = MutableStateFlow<UiData<String>>(UiData.None())
    val userData = _userData.asStateFlow()
    
    init {
        fetchUserData()
    }

    fun addFavorite(id: String, isFavorite: Boolean) {
        launchCoroutineScope {
            _bookUpdated.value = UiData.Loading()
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .document(id)
                .update(hashMapOf("favorite" to isFavorite) as Map<String, Any>)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        _bookUpdated.value = UiData.Success(isFavorite)
                    } else {
                        _bookUpdated.value = UiData.Error("Failed to add it to favorites - ${task.exception?.message}")
                    }
                }.addOnFailureListener {
                    _bookUpdated.value = UiData.Error("Failed to add it to favorites - ${it.message}")
                }
        }
    }

    fun fetchBooks() {
        launchCoroutineScope {
            _books.value = UiData.Loading()
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .get()
                .addOnSuccessListener { query ->
                    val books = ArrayList<FirebaseBook>()
                    query.forEach { document ->
                        val book = document.toObject(FirebaseBook::class.java)
                        books.add(book)
                    }
                    _books.value = UiData.Success(books.filter {
                        it.userId == FirebaseAuth.getInstance().currentUser?.uid
                    })
                }.addOnFailureListener {
                    _books.value = UiData.Error("Failed to get books - ${it.message}")
                }
        }
    }

    private fun fetchUserData() {
        launchCoroutineScope {
            _userData.value = UiData.Loading()
            FirebaseAuth.getInstance().currentUser?.let { firebaseUser ->
                FirebaseFirestore.getInstance()
                    .collection("users")
                    .get()
                    .addOnSuccessListener {
                        it.forEach { document ->
                            val user = document.toObject(User::class.java)
                            if (user.user_id == firebaseUser.uid) {
                                _userData.value = UiData.Success(user.name.fetchFirstName())
                            }
                        }
                    }.addOnFailureListener {
                        _userData.value = UiData.Error("Failed to get account details - ${it.message}")
                    }
            }
        }
    }

    override fun onCreate() {}
}