package com.compose.capstoneapp.viewmodel

import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.utils.UiData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class FavoriteBooksViewModel: ViewModelBase() {

    private val _books = MutableStateFlow<UiData<List<FirebaseBook>>>(UiData.None())
    val books = _books.asStateFlow()

    init {
        fetchFavoriteBooks()
    }

    private fun fetchFavoriteBooks() {
        launchCoroutineScope {
            _books.value = UiData.Loading()
            val db = FirebaseFirestore.getInstance()
            db.collection("books")
                .get()
                .addOnSuccessListener { query ->
                    val books = ArrayList<FirebaseBook>()
                    query.forEach { document ->
                        val book = document.toObject(FirebaseBook::class.java)
                        books.add(book)
                    }
                    _books.value = UiData.Success(books.filter {
                        it.userId == FirebaseAuth.getInstance().currentUser?.uid && it.isFavorite
                    })
                }.addOnFailureListener {
                    _books.value = UiData.Error("Failed to get books - ${it.message}")
                }
        }
    }

    override fun onCreate() {}
}