package com.compose.capstoneapp.viewmodel

import com.compose.capstoneapp.data.model.LoginResult
import com.compose.capstoneapp.data.model.User
import com.compose.capstoneapp.utils.UiData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class AuthViewModel: ViewModelBase() {

    private val auth = FirebaseAuth.getInstance()
    private val _loginResult = MutableStateFlow<UiData<LoginResult>>(UiData.None())
    val loginResult = _loginResult.asStateFlow()

    fun login(email: String, password: String) {
        launchCoroutineScope {
            _loginResult.value = UiData.Loading()
            auth.signInWithEmailAndPassword(
                email,
                password
            ).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    _loginResult.value = UiData.Success(
                        LoginResult(
                            true,
                            "You have successfully logged in."
                        )
                    )
                } else {
                    _loginResult.value = UiData.Error("Failed to log in - ${task.exception?.message}")
                }
            }.addOnFailureListener {
                _loginResult.value = UiData.Error("Failed to log in - ${it.message}")
            }
        }
    }

    fun signup(email: String, name: String, password: String) {
        launchCoroutineScope {
            _loginResult.value = UiData.Loading()
            auth.createUserWithEmailAndPassword(
                email,
                password
            ).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    auth.currentUser?.let { user ->
                        val mUser = User(email, name, user.uid)
                        FirebaseFirestore.getInstance()
                            .collection("users")
                            .add(mUser.toMap())
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    _loginResult.value = UiData.Success(LoginResult(true, "You have successfully created you account."))
                                } else {
                                    _loginResult.value = UiData.Error("Failed to create account - ${task.exception?.message}")
                                }
                            }
                    }
                } else {
                    _loginResult.value = UiData.Error("Failed to create account - ${task.exception?.message}")
                }
            }.addOnFailureListener {
                _loginResult.value = UiData.Error("Failed to create account - ${it.message}")
            }
        }
    }

    override fun onCreate() {}
}