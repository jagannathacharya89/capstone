package com.compose.capstoneapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CapstoneApplication: Application()