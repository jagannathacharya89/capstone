package com.compose.capstoneapp.ui.components

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material.icons.rounded.FavoriteBorder
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.compose.capstoneapp.R
import com.compose.capstoneapp.ui.navigation.Screens
import com.google.firebase.auth.FirebaseAuth

val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun EmailInputText(
    modifier: Modifier = Modifier,
    hint: String,
    input: String,
    isError: Boolean,
    onTextChange: (String) -> Unit
) {
    OutlinedTextField(
        modifier = modifier,
        isError = isError,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Next,
            keyboardType = KeyboardType.Email
        ),
        label = {
             Text(
                 fontFamily = fontFamily,
                 text = hint
             )
        },
        onValueChange = onTextChange,
        shape = RoundedCornerShape(10.dp),
        textStyle = TextStyle(fontFamily = fontFamily),
        trailingIcon = {
            if (isError) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_error),
                    contentDescription = "error",
                    tint = MaterialTheme.colorScheme.error
                )
            }
        },
        value = input
    )
}

@Composable
fun InputText(
    modifier: Modifier = Modifier,
    hint: String,
    input: String,
    isHint: Boolean,
    keyboardActions: KeyboardActions,
    keyboardOptions: KeyboardOptions,
    onTextChange: (String) -> Unit
) {
    OutlinedTextField(
        modifier = modifier,
        keyboardActions = keyboardActions,
        keyboardOptions = keyboardOptions,
        label = {
            if (isHint) {
                Text(
                    fontFamily = fontFamily,
                    text = hint
                )
            }
        },
        onValueChange = onTextChange,
        shape = RoundedCornerShape(10.dp),
        textStyle = TextStyle(fontFamily = fontFamily),
        value = input
    )
}

@Composable
fun PasswordInputText(
    modifier: Modifier = Modifier,
    hint: String,
    input: String,
    isToggle: Boolean,
    keyboardActions: KeyboardActions,
    onTextChange: (String) -> Unit,
    onToggle: () -> Unit
) {
    OutlinedTextField(
        modifier = modifier,
        keyboardActions = keyboardActions,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Password
        ),
        label = {
            Text(
                fontFamily = fontFamily,
                text = hint
            )
        },
        onValueChange = onTextChange,
        shape = RoundedCornerShape(10.dp),
        textStyle = TextStyle(fontFamily = fontFamily),
        trailingIcon = {
            val icon = if (isToggle) painterResource(id = R.drawable.ic_hide_pw)
            else painterResource(id = R.drawable.ic_show_pw)
            IconButton(onClick = onToggle) {
                Icon(
                    painter = icon,
                    contentDescription = "password_toggle"
                )
            }
        },
        value = input,
        visualTransformation = if (isToggle) VisualTransformation.None
        else PasswordVisualTransformation()
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Toolbar(
    navController: NavController,
    isHome: Boolean,
    title: String
) {
    TopAppBar(
        actions = {
            if (isHome) {
                IconButton(
                    onClick = {
                        navController.navigate(Screens.FavoriteScreen.name)
                    })
                {
                    Icon(
                        imageVector = Icons.Rounded.FavoriteBorder,
                        contentDescription = "log out"
                    )
                }
                IconButton(
                    onClick = {
                        FirebaseAuth.getInstance().signOut()
                        navController.navigate(Screens.LoginScreen.name) {
                            popUpTo(Screens.HomeScreen.name) {
                                inclusive = true
                            }
                        }
                    })
                {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_logout),
                        contentDescription = "log out"
                    )
                }
            }
        },
        navigationIcon = {
            if (isHome) {
                IconButton(
                    onClick = {}
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_profile),
                        contentDescription = "profile"
                    )
                }
            } else {
                IconButton(
                    onClick = {
                        navController.navigateUp()
                    }
                ) {
                    Icon(
                        imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                        contentDescription = "back"
                    )
                }
            }
        },
        title = {
            Text(
                fontFamily = fontFamily,
                fontSize = 16.sp,
                text = title
            )
        }
    )
}