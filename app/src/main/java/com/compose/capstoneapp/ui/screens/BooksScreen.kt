package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.compose.capstoneapp.R
import com.compose.capstoneapp.data.model.Book
import com.compose.capstoneapp.ui.components.InputText
import com.compose.capstoneapp.ui.components.Toolbar
import com.compose.capstoneapp.ui.navigation.Screens
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.joinString
import com.compose.capstoneapp.viewmodel.BooksViewModel

private val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun BooksScreen(
    navController: NavController,
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>()
) {
    val query = remember {
        mutableStateOf("")
    }
    Scaffold(
        topBar = {
            Toolbar(
                navController = navController,
                isHome = false,
                title = "Search Books"
            )
        }
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            val keyboardController = LocalSoftwareKeyboardController.current
            InputText(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        end = 12.dp,
                        start = 12.dp,
                        top = it.calculateTopPadding()
                    ),
                hint = "Search",
                input = query.value,
                isHint = true,
                keyboardActions = KeyboardActions(
                    onDone = {
                        if (query.value.isEmpty())
                            return@KeyboardActions
                        booksVM.fetchBooks(query.value)
                        keyboardController?.hide()
                    }
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Text
                ),
                onTextChange = { value ->
                    query.value = value
                }
            )
            Books(booksVM, navController)
        }
    }
}

@Composable
private fun BookCard(
    book: Book,
    navController: NavController
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                end = 12.dp,
                start = 12.dp,
                top = 12.dp
            ),
        colors = CardDefaults.cardColors(Color.White),
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(12.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    navController.navigate(Screens.BookDetailsScreen.name + "/${book.id}")
                },
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                modifier = Modifier
                    .padding(10.dp)
                    .size(
                        width = 70.dp,
                        height = 90.dp
                    ),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(book.volumeInfo.imageLinks.smallThumbnail)
                    .build(),
                contentDescription = "book"
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 10.dp),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 14.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = book.volumeInfo.title
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Authors: ${book.volumeInfo.authors.joinString()}"
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Date: ${book.volumeInfo.publishedDate}"
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = book.volumeInfo.categories.joinString()
                )
            }
        }
    }
}

@Composable
fun Books(
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>(),
    navController: NavController
) {
    when (val uiData = booksVM.books.collectAsState().value) {
        is UiData.Error -> {}
        is UiData.Loading -> {}
        is UiData.None -> {}
        is UiData.Success -> {
            if (uiData.data.isNotEmpty()) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentPadding = PaddingValues(bottom = 12.dp)
                ) {
                    items(uiData.data) { book ->
                        BookCard(book, navController)
                    }
                }
            }
        }
    }
}