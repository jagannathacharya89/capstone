package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.compose.capstoneapp.R
import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.ui.components.Toolbar
import com.compose.capstoneapp.ui.navigation.Screens
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.toDoubleOrZero
import com.compose.capstoneapp.utils.toStringOrNA
import com.compose.capstoneapp.utils.toast
import com.compose.capstoneapp.viewmodel.HomeViewModel

private val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun HomeScreen(
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
    navController: NavController
) {
    Scaffold(
        topBar = {
            HomeScreenHeader(
                homeVM = homeVM,
                navController = navController,
                isHome = true
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                containerColor = MaterialTheme.colorScheme.primary,
                elevation = FloatingActionButtonDefaults.elevation(4.dp),
                onClick = {
                    navController.navigate(Screens.BooksScreen.name)
                },
                shape = RoundedCornerShape(40.dp)
            ) {
                Icon(
                    imageVector = Icons.Rounded.Add,
                    contentDescription = "add",
                    tint = Color.White
                )
            }
        }
    ) {
        LaunchedEffect(key1 = Unit) {
            homeVM.fetchBooks()
        }
        val context = LocalContext.current
        when (val result = homeVM.books.collectAsState().value) {
            is UiData.Error -> context.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> HomeScreenContent(navController, result.data, homeVM, it)
        }
    }
}

@Composable
private fun BookCard(
    navController: NavController,
    book: FirebaseBook,
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
) {
    val context = LocalContext.current
    val isFavorite = remember {
        mutableStateOf(book.isFavorite)
    }
    val result = homeVM.bookUpdated.collectAsState().value
    LaunchedEffect(key1 = result) {
        when (result) {
            is UiData.Error -> context.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> {
                context.toast(if (result.data) "Marked as favorite." else "Removed from favorites.")
                isFavorite.value = result.data
            }
        }
    }
    Card(
        modifier = Modifier
            .padding(
                start = 12.dp,
                top = 16.dp
            )
            .width(width = 200.dp),
        colors = CardDefaults.cardColors(Color.White),
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(20.dp)
    ) {
        Column(
            modifier = Modifier
                .clickable {
                    navController.navigate(Screens.EditBookScreen.name + "/${book.title.toStringOrNA()}")
                }
                .fillMaxWidth(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Top
        ) {
            Box(modifier = Modifier.fillMaxWidth()) {
                AsyncImage(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(130.dp),
                    contentScale = ContentScale.FillBounds,
                    model = ImageRequest.Builder(context)
                        .data(book.imgUrl)
                        .build(),
                    contentDescription = "book"
                )
                Column(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(
                            end = 10.dp,
                            top = 6.dp
                        ),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Top
                ) {
                    Card(
                        colors = CardDefaults.cardColors(Color.White),
                        elevation = CardDefaults.cardElevation(2.dp),
                        shape = CircleShape
                    ) {
                        Surface(
                            modifier = Modifier.clickable {
                                isFavorite.value = !isFavorite.value
                                homeVM.addFavorite(book.id.toStringOrNA(), isFavorite.value)
                            },
                            color = Color.Transparent
                        ) {
                            Icon(
                                modifier = Modifier.padding(6.dp),
                                painter = painterResource(id = R.drawable.ic_favorite),
                                contentDescription = "favourite",
                                tint = if (isFavorite.value) Color.Red.copy(0.7f)
                                else Color(0xFF5f6368)
                            )
                        }
                    }
                    Surface(
                        modifier = Modifier.padding(top = 6.dp),
                        color = Color.White,
                        shadowElevation = 2.dp,
                        shape = CircleShape
                    ) {
                        Column(
                            modifier = Modifier.padding(
                                horizontal = 2.dp,
                                vertical = 4.dp
                            ),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_rating),
                                contentDescription = "rating"
                            )
                            Text(
                                modifier = Modifier.padding(top = 4.dp),
                                fontFamily = fontFamily,
                                fontSize = 12.sp,
                                text = book.rating.toDoubleOrZero()
                            )
                        }
                    }
                }
            }
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        end = 12.dp,
                        start = 12.dp,
                        top = 12.dp
                    ),
                fontFamily = fontFamily,
                fontSize = 14.sp,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                text = book.title.toStringOrNA()
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        end = 12.dp,
                        start = 12.dp
                    ),
                fontFamily = fontFamily,
                fontSize = 12.sp,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                text = book.authors.toStringOrNA()
            )
            Card(
                modifier = Modifier.align(Alignment.End),
                colors = CardDefaults.cardColors(MaterialTheme.colorScheme.primary),
                shape = RoundedCornerShape(
                    bottomEnd = 20.dp,
                    topStart = 20.dp
                )
            ) {
                Text(
                    modifier = Modifier.padding(
                        horizontal = 10.dp,
                        vertical = 4.dp
                    ),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = if (book.isReading) "Reading"
                    else "Not started"
                )
            }
        }
    }
}

@Composable
fun CurrentBook(
    navController: NavController,
    books: List<FirebaseBook>,
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            modifier = Modifier.padding(start = 12.dp),
            fontFamily = fontFamily,
            text = "Current reading activity"
        )
        if (books.isEmpty()) {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 30.dp),
                color = Color.Red.copy(alpha = 0.6f),
                fontFamily = fontFamily,
                fontSize = 12.sp,
                text = "There is no current reading activity."
            )
        }
        LazyRow(
            modifier = Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(
                bottom = 16.dp,
                end = 12.dp
            )
        ) {
            items(books) { book ->
                BookCard(navController, book, homeVM)
            }
        }
    }
}

@Composable
fun HomeScreenContent(
    navController: NavController,
    books: List<FirebaseBook>,
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
    values: PaddingValues
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = values.calculateTopPadding() + 10.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ) {
        CurrentBook(navController, books.filter { it.isReading && !it.isRead }, homeVM)
        YourBooks(navController, books.filter { !it.isReading && !it.isRead }, homeVM)
    }
}

@Composable
fun HomeScreenHeader(
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
    navController: NavController,
    isHome: Boolean
) {
    val context = LocalContext.current
    when (val result = homeVM.userData.collectAsState().value) {
        is UiData.Error -> context.toast(result.error)
        is UiData.Loading -> {}
        is UiData.None -> {}
        is UiData.Success -> {
            Toolbar(
                navController = navController,
                isHome = isHome,
                title = result.data
            )
        }
    }
}

@Composable
fun YourBooks(
    navController: NavController,
    books: List<FirebaseBook>,
    homeVM: HomeViewModel = viewModel<HomeViewModel>(),
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            modifier = Modifier.padding(
                start = 12.dp,
                top = 16.dp
            ),
            fontFamily = fontFamily,
            text = "Your books"
        )
        if (books.isEmpty()) {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 30.dp),
                color = Color.Red.copy(alpha = 0.6f),
                fontFamily = fontFamily,
                fontSize = 12.sp,
                text = "There is no saved books."
            )
        }
        LazyRow(
            modifier = Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(
                bottom = 16.dp,
                end = 12.dp
            )
        ) {
            items(books) { book ->
                BookCard(navController, book, homeVM)
            }
        }
    }
}