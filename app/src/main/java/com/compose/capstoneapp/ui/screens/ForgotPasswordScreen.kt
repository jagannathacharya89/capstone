package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.compose.capstoneapp.R
import com.compose.capstoneapp.utils.isValidEmail

@Composable
fun ForgotPasswordScreen(
    navController: NavController
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ) {
        val fontFamily = FontFamily(Font(R.font.work_sans_regular))
        val email = remember {
            mutableStateOf("")
        }
        val isEmailError = remember {
            mutableStateOf(false)
        }
        val isEnabled = remember(email.value) {
            email.value.isNotEmpty()
        }
        val keyboardController = LocalSoftwareKeyboardController.current
        Row(
            modifier = Modifier.padding(
                start = 16.dp,
                top = 30.dp
            ),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(
                onClick = {
                    navController.navigateUp()
                }
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                    contentDescription = "back"
                )
            }
            Text(
                modifier = Modifier.padding(start = 16.dp),
                fontFamily = fontFamily,
                fontSize = 22.sp,
                text = "Forgot Password"
            )
        }
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 20.dp
                ),
            isError = isEmailError.value,
            keyboardActions = KeyboardActions {
                keyboardController?.hide()
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Email
            ),
            label = {
                Text(
                    fontFamily = fontFamily,
                    text = "Email"
                )
            },
            onValueChange = {
                email.value = it
                isEmailError.value = false
            },
            textStyle = TextStyle(
                fontFamily = fontFamily,
            ),
            trailingIcon = {
                if (isEmailError.value) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_error),
                        contentDescription = "error",
                        tint = MaterialTheme.colorScheme.error
                    )
                }
            },
            shape = RoundedCornerShape(10.dp),
            value = email.value
        )
        if (isEmailError.value) {
            Text(
                modifier = Modifier
                    .wrapContentSize()
                    .padding(start = 12.dp),
                color = MaterialTheme.colorScheme.error,
                fontFamily = fontFamily,
                fontSize = 12.sp,
                text = "Not a valid email address"
            )
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 20.dp
                ),
            enabled = isEnabled,
            onClick = {
                isEmailError.value = !email.value.isValidEmail()
            },
            shape = RoundedCornerShape(10.dp)
        ) {
            Text(
                modifier = Modifier.padding(vertical = 8.dp),
                fontFamily = fontFamily,
                text = "Reset Password"
            )
        }
    }
}