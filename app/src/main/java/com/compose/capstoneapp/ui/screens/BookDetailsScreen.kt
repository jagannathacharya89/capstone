package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.compose.capstoneapp.R
import com.compose.capstoneapp.data.model.Book
import com.compose.capstoneapp.ui.components.Toolbar
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.formatHtml
import com.compose.capstoneapp.utils.joinString
import com.compose.capstoneapp.utils.toast
import com.compose.capstoneapp.viewmodel.BooksViewModel

private val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun BookDetailsScreen(
    navController: NavController,
    bookId: String,
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>()
) {
    Scaffold(
        topBar = {
            Toolbar(
                navController = navController,
                isHome = false,
                title = "Details"
            )
        }
    ) {
        LaunchedEffect(key1 = Unit) {
            booksVM.fetchBookInfo(bookId)
        }
        val context = LocalContext.current
        when (val result = booksVM.bookInfo.collectAsState().value) {
            is UiData.Error -> context.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> BookDetails(navController, result.data, booksVM, it.calculateTopPadding())
        }
    }
}

@Composable
fun BookDetails(
    navController: NavController,
    book: Book,
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>(),
    topPadding: Dp
) {
    val context = LocalContext.current
    val result = booksVM.bookSaved.collectAsState().value
    LaunchedEffect(key1 = result) {
        when (result) {
            is UiData.Error -> context.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> {
                context.toast("Book has been added successfully.")
                navController.navigateUp()
            }
        }
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = topPadding),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        AsyncImage(
            modifier = Modifier
                .padding(top = 10.dp)
                .size(
                    width = 70.dp,
                    height = 70.dp
                ),
            model = ImageRequest.Builder(LocalContext.current)
                .data(book.volumeInfo.imageLinks.smallThumbnail)
                .transformations(CircleCropTransformation())
                .build(),
            contentDescription = "book"
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 30.dp
                ),
            fontFamily = fontFamily,
            fontSize = 18.sp,
            text = book.volumeInfo.title,
            textAlign = TextAlign.Center
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 6.dp
                ),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Authors: ${book.volumeInfo.authors.joinString()}",
            textAlign = TextAlign.Center
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 4.dp
                ),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Page Count: ${book.volumeInfo.pageCount}",
            textAlign = TextAlign.Center
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 4.dp
                ),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Categories: ${book.volumeInfo.categories.joinString()}",
            textAlign = TextAlign.Center
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 4.dp
                ),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Published By: ${book.volumeInfo.publisher}",
            textAlign = TextAlign.Center
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 4.dp
                ),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Published On: ${book.volumeInfo.publishedDate}",
            textAlign = TextAlign.Center
        )
        val localDims = LocalContext.current.resources.displayMetrics
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .height(localDims.heightPixels.dp.times(0.09f))
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 10.dp
                ),
            border = BorderStroke(1.dp, Color.DarkGray),
            shape = RoundedCornerShape(8.dp)
        ) {
            LazyColumn(
                modifier = Modifier.padding(6.dp)
            ) {
                item {
                    Text(
                        fontFamily = fontFamily,
                        fontSize = 12.sp,
                        text = book.volumeInfo.description.formatHtml()
                    )
                }
            }
        }
        Row(
            modifier = Modifier.padding(top = 10.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(
                modifier = Modifier.padding(end = 10.dp),
                onClick = {
                    booksVM.addBook(book)
                },
                shape = RoundedCornerShape(6.dp)
            ) {
                Text(
                    fontFamily = fontFamily,
                    text = "Save"
                )
            }
            Button(
                modifier = Modifier.padding(start = 10.dp),
                onClick = {
                    navController.navigateUp()
                },
                shape = RoundedCornerShape(6.dp)
            ) {
                Text(
                    fontFamily = fontFamily,
                    text = "Cancel"
                )
            }
        }
    }
}