package com.compose.capstoneapp.ui.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.compose.capstoneapp.ui.screens.BookDetailsScreen
import com.compose.capstoneapp.ui.screens.BooksScreen
import com.compose.capstoneapp.ui.screens.EditBookScreen
import com.compose.capstoneapp.ui.screens.FavoriteScreen
import com.compose.capstoneapp.ui.screens.ForgotPasswordScreen
import com.compose.capstoneapp.ui.screens.HomeScreen
import com.compose.capstoneapp.ui.screens.LoginScreen
import com.compose.capstoneapp.ui.screens.SignupScreen
import com.compose.capstoneapp.ui.screens.SplashScreen
import com.compose.capstoneapp.viewmodel.BooksViewModel
import com.compose.capstoneapp.viewmodel.AuthViewModel
import com.compose.capstoneapp.viewmodel.FavoriteBooksViewModel
import com.compose.capstoneapp.viewmodel.HomeViewModel

@Composable
fun Navigation() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.SplashScreen.name
    ) {
        composable(
            Screens.SplashScreen.name
        ) {
            SplashScreen(navController = navController)
        }
        composable(
            Screens.LoginScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            val authVM = viewModel<AuthViewModel>()
            LoginScreen(authVM, navController)
        }
        composable(
            Screens.SignupScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            val authVM = viewModel<AuthViewModel>()
            SignupScreen(authVM, navController)
        }
        composable(
            Screens.ForgotPasswordScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            ForgotPasswordScreen(navController = navController)
        }
        composable(
            Screens.HomeScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            val homeVM = viewModel<HomeViewModel>()
            HomeScreen(homeVM, navController)
        }
        composable(
            Screens.BooksScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            val booksVM = hiltViewModel<BooksViewModel>()
            BooksScreen(navController, booksVM)
        }
        composable(
            "${Screens.BookDetailsScreen.name}/{bookId}",
            arguments = listOf(navArgument("bookId") {
                type = NavType.StringType
            }),
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) { backstackEntry ->
            val bookId = backstackEntry.arguments?.getString("bookId").toString()
            val booksVM = hiltViewModel<BooksViewModel>()
            BookDetailsScreen(navController, bookId, booksVM)
        }
        composable(
            "${Screens.EditBookScreen.name}/{bookTitle}",
            arguments = listOf(navArgument("bookTitle") {
                type = NavType.StringType
            }),
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) { backstackEntry ->
            val bookType = backstackEntry.arguments?.getString("bookTitle").toString()
            val booksVM = hiltViewModel<BooksViewModel>()
            EditBookScreen(navController, bookType, booksVM)
        }
        composable(
            Screens.FavoriteScreen.name,
            enterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Start,
                    tween(500)
                )
            },
            popEnterTransition = {
                return@composable slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.End,
                    tween(500)
                )
            }
        ) {
            val favoriteBooksVM = viewModel<FavoriteBooksViewModel>()
            FavoriteScreen(navController, favoriteBooksVM)
        }
    }
}