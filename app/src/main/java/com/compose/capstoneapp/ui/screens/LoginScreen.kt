package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.compose.capstoneapp.R
import com.compose.capstoneapp.ui.components.EmailInputText
import com.compose.capstoneapp.ui.components.PasswordInputText
import com.compose.capstoneapp.ui.navigation.Screens
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.isValidEmail
import com.compose.capstoneapp.utils.toast
import com.compose.capstoneapp.viewmodel.AuthViewModel

@Composable
fun LoginScreen(
    authVM: AuthViewModel = viewModel<AuthViewModel>(),
    navController: NavController
) {
    val context = LocalContext.current
    val isLoading = remember {
        mutableStateOf(false)
    }
    val uiData = authVM.loginResult.collectAsState().value
    LaunchedEffect(key1 = uiData) {
        when (uiData) {
            is UiData.Error -> {
                context.toast(uiData.error)
                isLoading.value = false
            }
            is UiData.Loading -> isLoading.value = true
            is UiData.None -> {}
            is UiData.Success -> {
                isLoading.value = false
                if (uiData.data.isSucess) {
                    context.toast(uiData.data.message)
                    navController.navigate(Screens.HomeScreen.name) {
                        popUpTo(Screens.LoginScreen.name) {
                            inclusive = true
                        }
                    }
                }
            }
        }
    }
    LoginScreenContent(
        authVM,
        isLoading,
        navController
    )
}

@Composable
fun LoginScreenContent(
    authVM: AuthViewModel = viewModel<AuthViewModel>(),
    isLoading: MutableState<Boolean>,
    navController: NavController
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ) {
        val email = remember {
            mutableStateOf("")
        }
        val fontFamily = FontFamily(Font(R.font.work_sans_regular))
        val isEmailError = remember {
            mutableStateOf(false)
        }
        val isPasswordVisible = remember {
            mutableStateOf(false)
        }
        val keyboardController = LocalSoftwareKeyboardController.current
        val password = remember {
            mutableStateOf("")
        }
        val isEnabled = remember(
            email.value,
            password.value
        ) {
            email.value.isNotEmpty() && password.value.isNotEmpty()
        }
        Text(
            modifier = Modifier.padding(
                start = 16.dp,
                top = 30.dp
            ),
            fontFamily = fontFamily,
            fontSize = 22.sp,
            text = "Login"
        )
        EmailInputText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 20.dp
                ),
            hint = "Email",
            input = email.value,
            isError = isEmailError.value,
            onTextChange = {
                email.value = it
                isEmailError.value = false
            }
        )
        if (isEmailError.value) {
            Text(
                modifier = Modifier
                    .wrapContentSize()
                    .padding(start = 12.dp),
                color = MaterialTheme.colorScheme.error,
                fontFamily = fontFamily,
                fontSize = 10.sp,
                text = "Not a valid email address"
            )
        }
        PasswordInputText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 10.dp
                ),
            hint = "Password",
            input = password.value,
            isToggle = isPasswordVisible.value,
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                    if (isEnabled)
                        authVM.login(email.value, password.value)
                }
            ),
            onTextChange = {
                password.value = it
            },
            onToggle = {
                isPasswordVisible.value = !isPasswordVisible.value
            }
        )
        Text(
            modifier = Modifier
                .wrapContentWidth()
                .align(Alignment.End)
                .padding(
                    end = 12.dp,
                    top = 12.dp
                )
                .clickable {
                    navController.navigate(Screens.ForgotPasswordScreen.name)
                },
            color = MaterialTheme.colorScheme.primary,
            fontFamily = fontFamily,
            fontSize = 12.sp,
            text = "Forgot Password?"
        )
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            enabled = isEnabled,
            onClick = {
                if (!isLoading.value) {
                    isEmailError.value = !email.value.isValidEmail()
                    if (!isEmailError.value) {
                        authVM.login(email.value, password.value)
                        keyboardController?.hide()
                    }
                }
            },
            shape = RoundedCornerShape(10.dp)
        ) {
            if (isLoading.value) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .padding(vertical = 8.dp)
                        .size(20.dp),
                    color = Color.White
                )
            } else {
                Text(
                    modifier = Modifier.padding(vertical = 8.dp),
                    fontFamily = fontFamily,
                    text = "Login"
                )
            }
        }
        Row(
            modifier = Modifier
                .wrapContentSize()
                .align(Alignment.CenterHorizontally)
                .padding(top = 10.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                fontFamily = fontFamily,
                fontSize = 14.sp,
                text = "Don't have an account?"
            )
            Text(
                modifier = Modifier
                    .padding(start = 8.dp)
                    .clickable {
                        navController.navigate(Screens.SignupScreen.name)
                    },
                color = MaterialTheme.colorScheme.primary,
                fontFamily = fontFamily,
                fontSize = 14.sp,
                text = "Sign Up"
            )
        }
    }
}