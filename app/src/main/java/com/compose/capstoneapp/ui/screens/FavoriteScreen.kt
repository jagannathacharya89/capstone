package com.compose.capstoneapp.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.compose.capstoneapp.R
import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.ui.components.Toolbar
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.toStringOrNA
import com.compose.capstoneapp.utils.toast
import com.compose.capstoneapp.viewmodel.FavoriteBooksViewModel

private val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun FavoriteScreen(
    navController: NavController,
    favoriteBooksVM: FavoriteBooksViewModel = viewModel<FavoriteBooksViewModel>()
) {
    Scaffold(
        topBar = {
            Toolbar(
                navController = navController,
                isHome = false,
                title = "Favorites"
            )
        }
    ) {
        when (val result = favoriteBooksVM.books.collectAsState().value) {
            is UiData.Error -> LocalContext.current.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> {
                FavoriteBooks(books = result.data, values = it)
            }
        }
    }
}

@Composable
fun FavoriteBooks(
    books: List<FirebaseBook>,
    values: PaddingValues
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = values.calculateTopPadding())
    ) {
        items(books) { book ->
            FavoriteBook(book)
        }
    }
}

@Composable
private fun FavoriteBook(book: FirebaseBook) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                end = 12.dp,
                start = 12.dp,
                top = 12.dp
            ),
        colors = CardDefaults.cardColors(Color.White),
        elevation = CardDefaults.cardElevation(4.dp),
        shape = RoundedCornerShape(12.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                modifier = Modifier
                    .padding(10.dp)
                    .size(
                        width = 70.dp,
                        height = 90.dp
                    ),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(book.imgUrl)
                    .build(),
                contentDescription = "book"
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 10.dp),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 14.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = book.title.toStringOrNA()
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Authors: ${book.authors}"
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Date: ${book.publishedDate.toStringOrNA()}"
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    text = book.categories.toStringOrNA()
                )
            }
        }
    }
}