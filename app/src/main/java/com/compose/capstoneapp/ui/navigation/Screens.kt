package com.compose.capstoneapp.ui.navigation

enum class Screens {
    SplashScreen,
    LoginScreen,
    SignupScreen,
    ForgotPasswordScreen,
    HomeScreen,
    BooksScreen,
    BookDetailsScreen,
    EditBookScreen,
    FavoriteScreen
}