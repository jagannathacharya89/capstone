package com.compose.capstoneapp.ui.screens

import android.view.MotionEvent
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Done
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.compose.capstoneapp.R
import com.compose.capstoneapp.data.model.FirebaseBook
import com.compose.capstoneapp.ui.components.InputText
import com.compose.capstoneapp.ui.components.Toolbar
import com.compose.capstoneapp.utils.UiData
import com.compose.capstoneapp.utils.formatTimeStamp
import com.compose.capstoneapp.utils.toIntOrZero
import com.compose.capstoneapp.utils.toStringOrNA
import com.compose.capstoneapp.utils.toast
import com.compose.capstoneapp.viewmodel.BooksViewModel

private val fontFamily = FontFamily(Font(R.font.work_sans_regular))

@Composable
fun EditBookScreen(
    navController: NavController,
    title: String,
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>()
) {
    Scaffold(
        topBar = {
            Toolbar(
                navController = navController,
                isHome = false,
                title = "Details"
            )
        }
    ) {
        LaunchedEffect(key1 = Unit) {
            booksVM.fetchBookInfoFromFirebase(title)
        }
        when (val result = booksVM.firebaseBookInfo.collectAsState().value) {
            is UiData.Error -> LocalContext.current.toast(result.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> EditBookScreenContent(
                navController,
                it,
                result.data,
                booksVM
            )
        }
    }
}

@Composable
fun EditBookScreenContent(
    navController: NavController,
    values: PaddingValues,
    book: FirebaseBook,
    booksVM: BooksViewModel = hiltViewModel<BooksViewModel>()
) {
    val comments = remember {
        mutableStateOf(book.notes?.ifEmpty { "No thoughts!" } ?: "No thoughts!")
    }
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val markAsRead = remember {
        mutableStateOf(book.isRead)
    }
    val rating = remember {
        mutableIntStateOf(book.rating.toIntOrZero())
    }
    val showAlertDialog = remember {
        mutableStateOf(false)
    }
    val startReading = remember {
        mutableStateOf(book.isReading)
    }
    val deletedResult = booksVM.bookDeleted.collectAsState().value
    val updatedResult = booksVM.bookUpdated.collectAsState().value
    LaunchedEffect(
        key1 = deletedResult,
        key2 = updatedResult
    ) {
        when (deletedResult) {
            is UiData.Error -> context.toast(deletedResult.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> {
                context.toast("Book deleted successfully.")
                navController.navigateUp()
                showAlertDialog.value = false
            }
        }
        when (updatedResult) {
            is UiData.Error -> context.toast(updatedResult.error)
            is UiData.Loading -> {}
            is UiData.None -> {}
            is UiData.Success -> {
                context.toast("Book details updated successfully.")
                navController.navigateUp()
            }
        }
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = values.calculateTopPadding()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                modifier = Modifier
                    .size(
                        width = 90.dp,
                        height = 90.dp
                    )
                    .padding(
                        start = 12.dp
                    ),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(book.imgUrl)
                    .transformations(CircleCropTransformation())
                    .build(),
                contentDescription = "book",
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        end = 12.dp,
                        start = 10.dp
                    ),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    fontFamily = fontFamily,
                    fontSize = 16.sp,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    text = book.title.toStringOrNA()
                )
                Text(
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Authors: ${book.authors.toStringOrNA()}"
                )
                Text(
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Published On: ${book.publishedDate.toStringOrNA()}"
                )
            }
        }
        InputText(
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
                .padding(
                    end = 12.dp,
                    start = 12.dp,
                    top = 14.dp
                ),
            hint = "Enter your thoughts",
            input = comments.value,
            isHint = true,
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            ),
            onTextChange = {
                comments.value = it
            }
        )
        Row(
            modifier = Modifier.padding(
                end = 12.dp,
                start = 12.dp,
                top = 12.dp
            ),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (book.isReading) {
                Text(
                    modifier = Modifier.padding(end = 6.dp),
                    color = Color.Gray,
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Started Reading: ${book.startTime.formatTimeStamp()}"
                )
            } else {
                Button(
                    modifier = Modifier.padding(end = 6.dp),
                    colors = ButtonDefaults.buttonColors(Color.Transparent),
                    onClick = {
                        markAsRead.value = false
                        startReading.value = !startReading.value
                    }
                ) {
                    if (startReading.value) {
                        Row(
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                color = MaterialTheme.colorScheme.primary,
                                fontFamily = fontFamily,
                                fontSize = 14.sp,
                                text = "Start Reading"
                            )
                            Image(
                                modifier = Modifier.padding(start = 10.dp),
                                imageVector = Icons.Rounded.Done,
                                colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.primary),
                                contentDescription = "done"
                            )
                        }
                    } else {
                        Text(
                            color = MaterialTheme.colorScheme.primary,
                            fontFamily = fontFamily,
                            fontSize = 14.sp,
                            text = "Start Reading"
                        )
                    }
                }
            }
            if (book.isRead) {
                Text(
                    modifier = Modifier.padding(start = 6.dp),
                    color = Color.Gray,
                    fontFamily = fontFamily,
                    fontSize = 12.sp,
                    text = "Reading Ended: ${book.endTime.formatTimeStamp()}"
                )
            }
            else {
                Button(
                    modifier = Modifier.padding(start = 6.dp),
                    colors = ButtonDefaults.buttonColors(Color.Transparent),
                    onClick = {
                        markAsRead.value = !markAsRead.value
                        startReading.value = true
                    }
                ) {
                    if (markAsRead.value) {
                        Row(
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                color = MaterialTheme.colorScheme.primary,
                                fontFamily = fontFamily,
                                fontSize = 14.sp,
                                text = "Mark as Read"
                            )
                            Image(
                                modifier = Modifier.padding(start = 10.dp),
                                imageVector = Icons.Rounded.Done,
                                colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.primary),
                                contentDescription = "done"
                            )
                        }
                    } else {
                        Text(
                            color = MaterialTheme.colorScheme.primary,
                            fontFamily = fontFamily,
                            fontSize = 14.sp,
                            text = "Mark as Read"
                        )
                    }
                }
            }
        }
        Text(
            modifier = Modifier.padding(top = 12.dp),
            fontFamily = fontFamily,
            fontSize = 14.sp,
            text = "Rating"
        )
        RatingBar(
            modifier = Modifier.padding(
                top = 12.dp
            ),
            rating = rating.intValue,
            onSelectRating = {
                rating.intValue = it
            }
        )
        Row(
            modifier = Modifier.padding(top = 12.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(
                modifier = Modifier.padding(end = 6.dp),
                onClick = {
                    booksVM.updateBook(
                        book.id.toStringOrNA(),
                        comments.value.ifEmpty { "No thoughts!" },
                        markAsRead.value,
                        startReading.value,
                        rating.intValue,
                        book.endTime,
                        book.startTime
                    )
                },
                shape = RoundedCornerShape(6.dp)
            ) {
                Text(
                    fontFamily = fontFamily,
                    fontSize = 14.sp,
                    text = "Update"
                )
            }
            Button(
                modifier = Modifier.padding(start =  6.dp),
                onClick = {
                    showAlertDialog.value = !showAlertDialog.value
                },
                shape = RoundedCornerShape(6.dp)
            ) {
                Text(
                    fontFamily = fontFamily,
                    fontSize = 14.sp,
                    text = "Delete"
                )
            }
            if (showAlertDialog.value) {
                AlertDialog(
                    onDismissRequest = {
                        showAlertDialog.value = false
                    },
                    confirmButton = {
                        Button(
                            onClick = {
                                booksVM.deleteBook(book.id.toStringOrNA())
                            }
                        ) {
                            Text(
                                fontFamily = fontFamily,
                                fontSize = 14.sp,
                                text = "Ok, Delete"
                            )
                        }
                    },
                    dismissButton = {
                        Button(
                            onClick = {
                                showAlertDialog.value = false
                            }
                        ) {
                            Text(
                                fontFamily = fontFamily,
                                fontSize = 14.sp,
                                text = "Cancel"
                            )
                        }
                    },
                    text = {
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            fontFamily = fontFamily,
                            fontSize = 12.sp,
                            text = "Are you sure you want to delete the subject? It can not be undone once deleted."
                        )
                    },
                    title = {
                        Text(
                            fontFamily = fontFamily,
                            fontSize = 14.sp,
                            text = "Delete Book"
                        )
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun RatingBar(
    modifier: Modifier,
    rating: Int,
    onSelectRating: (Int) -> Unit
) {
    val ratingState = remember {
        mutableIntStateOf(rating)
    }
    val selected = remember {
        mutableStateOf(false)
    }
    val size by animateDpAsState(
        targetValue = if (selected.value) 42.dp else 34.dp,
        spring(Spring.DampingRatioMediumBouncy),
        label = ""
    )
    Row(
        modifier = Modifier.width(280.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        for (i in 1..5) {
            Icon(
                modifier = modifier
                    .size(size)
                    .pointerInteropFilter {
                        when (it.action) {
                            MotionEvent.ACTION_DOWN -> {
                                selected.value = true
                                onSelectRating.invoke(i)
                                ratingState.intValue = i
                            }

                            MotionEvent.ACTION_UP -> {
                                selected.value = false
                            }
                        }
                        true
                    },
                imageVector = Icons.Rounded.Star,
                contentDescription = "star",
                tint = if (i <= ratingState.intValue) Color(0xFFFFD700)
                else Color(0xFF000000)
            )
        }
    }
}